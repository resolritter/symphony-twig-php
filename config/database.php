<?php

namespace AppConfig;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

class Database
{
	private static $config;
	private static $connectionParams;

	private static function startParameters()
	{
		if (self::$config == null) {
			self::$config = new Configuration();
		}
		if (self::$connectionParams == null) {
			self::$connectionParams = [
				'path' => 'mix-internet.sqlite',
				'driver' => 'pdo_sqlite'
			];
		}
	}

	public static function getConnection()
	{
		self::startParameters();
		return DriverManager::getConnection(self::$connectionParams, self::$config);
	}
}
