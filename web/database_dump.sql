--
-- File generated with SQLiteStudio v3.1.1 on Mon Jul 24 01:51:41 2017
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: curriculo
DROP TABLE IF EXISTS curriculo;

CREATE TABLE curriculo (
    id              INTEGER         PRIMARY KEY AUTOINCREMENT,
    nome_arquivo    VARCHAR (0, 80) NOT NULL,
    caminho_arquivo TEXT            NOT NULL
);


-- Table: formulario
DROP TABLE IF EXISTS formulario;

CREATE TABLE formulario (
    id             INTEGER          PRIMARY KEY AUTOINCREMENT,
    nome           VARCHAR (0, 100) NOT NULL,
    email          VARCHAR (0, 100) NOT NULL,
    telefone       VARCHAR (0, 20)  NOT NULL,
    cargo_desejado TEXT             NOT NULL,
    escolaridade   VARCHAR (0, 80)  NOT NULL,
    observacoes    TEXT,
    curriculo      INTEGER          REFERENCES curriculo (id) ON DELETE CASCADE
                                    NOT NULL,
    data_do_envio  DATETIME         NOT NULL
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
