<?php

namespace Controllers;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class CurriculoController
{
    public static function handleFile(UploadedFile $file)
    {
        $path = __DIR__.'/../../web/uploads/';
        $filename = $file->getClientOriginalName();
        $file->move($path,$filename);

        return [
            "nome_arquivo" => $filename,
            "caminho_arquivo" => "/web/uploads/"
        ];
    }
}
