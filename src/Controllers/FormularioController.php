<?php
namespace Controllers;

use Silex\Application;
use AppConfig\Database;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class FormularioController
{
    public static function filter($params = [])
    {
        $connection = Database::getConnection();
        $query = "SELECT formulario.id, formulario.nome, curriculo.nome_arquivo, curriculo.caminho_arquivo" .
            " FROM formulario INNER JOIN curriculo ON formulario.curriculo = curriculo.id";
        $statement = null;
        if (sizeof($params) != 0) {
            $query .= " WHERE ";
            foreach (array_keys($params) as $key) {
                $query .= sprintf("%s like :%s AND", $key, $key);
            }
            $statement = $connection->prepare(substr($query, 0, strrpos($query, "AND")));
            foreach ($params as $key => $value) {
                $statement->bindValue($key, '%' . $value . '%');
            }
        } else {
            $statement = $connection->prepare($query);
        }
        $statement->execute();

        return $statement->fetchAll();
    }

    public static function handleFormData($formData, $params)
    {
        return array_merge($formData, $params);
    }

    public static function generateForm(Application $app)
    {
        return $app['form.factory']
            ->createBuilder(FormType::class, null, [
                "label" => "Formulário de submissão de currículos"
            ])
            ->add('nome', TextType::class, [
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "Nome"
                ],
                "constraints" => [
                    new NotBlank(["message" => "Campo nome está vazio!"])
                ]
            ])
            ->add('email', EmailType::class, [
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "E-mail"
                ],
                "constraints" => [
                    new NotBlank(["message" => "Campo email está vazio!"]),
                    new Email(["message" => "Formato de e-mail inválido!"])
                ]
            ])
            ->add('telefone', TextType::class, [
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "Telefone"
                ],
                "constraints" => [
                    new NotBlank(["message" => "Campo telefone está vazio!"])
                ]
            ])
            ->add('cargo_desejado', TextareaType::class, [
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "Cargo desejado"
                ],
                "constraints" => [
                    new NotBlank(["message" => "Campo cargo desejado está vazio!"])
                ]
            ])
            ->add('escolaridade', ChoiceType::class, [
                "attr" => [
                  "class" => "form-control"
                ],
                "label" => "Escolaridade",
                'choices' => ['Não-graduado' => "nao_grad", 'Graduado' => "grad", 'Pós-graduado' => "pos_grad"],
                "constraints" => [
                    "choices" => ["nao_grad","grad","pos_grad"],
                    "message" => "Opção de escolaridade inválida"
                ]
            ])
            ->add('observacoes', TextareaType::class, [
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "Observações"
                ],
                "required" => false,
                "label" => "Observações"
            ])
            ->add('arquivo', FileType::class, [
                "attr" => [
                    "accept" => "*.doc,*.docx,*.pdf"
                ],
                "constraints" => [
                    new File([
                        "maxSize" => "1m",
                        "mimeTypes" => [
                            "application/pdf",
                            "application/msword",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                        ],
                        "maxSizeMessage" => "Tamanho máximo do arquivo é 1MB!",
                        "mimeTypesMessage" => "O arquivo não está nos formatos permitidos!"
                    ])
                ]
            ])
            ->add('data_do_envio', HiddenType::class)
            ->add('submit', SubmitType::class, [
                "attr" => [
                    "class" => "form-control btn"
                ],
                'label' => 'Enviar'
            ]);
    }
}
