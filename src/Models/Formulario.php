<?php

namespace Models;

class Formulario extends BaseModel
{
    const TABLE = "formulario";

    protected $id;
    protected $nome;
    protected $email;
    protected $telefone;
    protected $cargo_desejado;
    protected $escolaridade;
    protected $observacoes;
    protected $curriculo;
    protected $data_do_envio;

    public function delete()
    {
        Curriculo::find($this->curriculo)->delete();

        return parent::delete();
    }

}
