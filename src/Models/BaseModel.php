<?php

namespace Models;

use AppConfig\Database;

class BaseModel implements \ArrayAccess
{
    const TABLE = "";

    public function __construct($params = [])
    {
        foreach (array_keys($params) as $key) {
            $this->$key = $params[$key];
        }
    }

    public function delete()
    {
        $connection = Database::getConnection();
        $class = get_called_class();
        $query = "DELETE FROM " . $class::TABLE . " WHERE id = :id";
        $statement = $connection->prepare($query);
        $statement->bindValue("id", $this->id);

        return $statement->execute();
    }

    public function update($newValues)
    {
        $connection = Database::getConnection();
        $class = get_called_class();
        $query = "UPDATE " . $class::TABLE . " SET ";
        foreach (array_keys($newValues) as $key) {
            $query .= sprintf("%s = :%s,", $key, $key);
        }
        $query = substr($query, 0, strlen($query)-1) . " WHERE id = :id";
        $statement = $connection->prepare($query);
        foreach ($newValues as $key => $value) {
            $statement->bindValue($key, $value);
        }
        $statement->bindValue("id", $this->id);

        return $statement->execute();
    }

    public static function insert($data)
    {
        $connection = Database::getConnection();
        $class = get_called_class();
        $connection->insert($class::TABLE, $data);

        return $connection->lastInsertId();
    }

    public static function find($id)
    {
        $connection = Database::getConnection();
        $class = get_called_class();
        $query = "SELECT * FROM " . $class::TABLE . " WHERE id = :id";
        $statement = $connection->prepare($query);
        $statement->bindValue("id", $id);
        $statement->execute();

        return new $class($statement->fetchAll()[0]);
    }

    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetUnset($offset)
    {
        $this->$offset = null;
    }
}
