<?php

namespace Models;

use AppConfig\Database;

class Curriculo extends BaseModel
{
    const TABLE = "curriculo";

    protected $id;
    protected $nome_arquivo;
    protected $caminho_arquivo;

    public static function getCurriculoFromUser($userID)
    {
        $connection = Database::getConnection();
        $query = "SELECT curriculo.*" .
            " FROM formulario" .
            " INNER JOIN curriculo ON formulario.curriculo = curriculo.id" .
            " WHERE formulario.id = :formulario_id";
        $statement = $connection->prepare($query);
        $statement->bindValue("formulario_id", $userID);
        $statement->execute();
        $fileData = $statement->fetchAll()[0];

        return sprintf("%s%s", substr($fileData["caminho_arquivo"], stripos($fileData["caminho_arquivo"],
                '/web/')+4), $fileData["nome_arquivo"]);
    }
}
