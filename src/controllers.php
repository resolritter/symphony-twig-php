<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Controllers\FormularioController;
use Controllers\CurriculoController;
use Models\Curriculo;
use Models\Formulario;

$app->get('/', function () use ($app) {
    $form = FormularioController::generateForm($app)->getForm();

    return $app['twig']->render('main.html.twig', array("form" => $form->createView()));
})
    ->bind('homepage');

$app->get('/{action}/{userID}', function (Request $request, $action, $userID) use ($app) {
    switch ($action) {
        case 'download_curriculum':
            return $app->redirect(Curriculo::getCurriculoFromUser($userID));
            break;
        case 'edit':
            $form = FormularioController::generateForm($app)->setData(Formulario::find($userID))->getForm();
            return $app['twig']->render('main.html.twig', array('form' => $form->createView(), "editUser" => $userID));
            break;
        case 'delete':
            if (Formulario::find($userID)->delete()) {
                $app['session']->getFlashBag()->add('message', 'O usuário foi deletado com sucesso!');
            } else {
                $app['session']->getFlashBag()->add('message', 'Falha na exclusão do usuário!');
            }
            return $app->redirect('/listar_formularios');
            break;
        default:
            return $app->redirect('/');
            break;
    }
})
    ->bind("userActions");

$app->get('/listar_formularios', function (Request $request) use ($app) {
    $dataList = FormularioController::filter($request->query->all());

    return $app['twig']->render('main.html.twig', ["dataList" => $dataList]);
})
    ->bind('listFormularios');

$app->post('/validar_formulario/{mode}/{userID}', function (Request $request, $mode, $userID) use ($app) {
    $form = FormularioController::generateForm($app)->getForm();
    $form->handleRequest($request);
    if ($form->isValid()) {
        $data = $form->getData();

        // inserção do arquivo
        $curriculo = $request->files->get($form->getName())['arquivo'];
        $curriculoData = CurriculoController::handleFile($curriculo);
        $lastCurriculumId = Curriculo::insert($curriculoData);

        // inserção do formulário por completo
        unset($data['arquivo']);
        $formData = FormularioController::handleFormData($data, [
            "curriculo" => $lastCurriculumId
        ]);
        if ($mode == "salvar") {
            Formulario::insert($formData);
            $app['session']->getFlashBag()->add('message', 'Nova submissão foi inserida no banco com sucesso.');
        } else {
            Formulario::find($userID)->update($data);
            $app['session']->getFlashBag()->add('message', 'O submetente foi atualizado no banco com sucesso.');
        }

        return $app->redirect('/listar_formularios');
    } else {
        if ($mode == "salvar") {
            return $app['twig']->render('main.html.twig', array('form' => $form->createView()));
        } else {
            return $app['twig']->render('main.html.twig', array('form' => $form->createView(), "editUser" => $userID));
        }
    }
})
    ->bind('validateFormData');

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
